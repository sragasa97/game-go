# GameGo

>> GameGo is a business-facing application designed to allow companies that sell games to efficiently log their inventory.
The application includes a microservice to allow company employees a chance to buy games from the company without having to use the consumer portal.

>> It uses the RAWG Video Game Database API to let employees easily populate their inventory with relevant information.
