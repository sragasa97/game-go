import { NavLink } from 'react-router-dom';
import {ReactComponent as GameGoLogoNav} from './static/gamego-website-favicon-color.svg'
import './nav.css'

function Nav() {
  return (
    <div className="row navfix">
    <nav className="navbar fixed-top navbar-expand-md navbar-dark bg-success px-0">
      <div className="container-fluid">
        {/* Logo */}
        <NavLink className="navbar-brand" to="/"><GameGoLogoNav className="gamegologonav d-inline-block"/></NavLink>
        {/* Hamburger Toggler */}
        <button className="navbar-toggler" type="button"
        data-bs-toggle="collapse"
        data-bs-target="#mainNavigation"
        aria-controls="mainNavigation"
        aria-expanded="false"
        aria-label="Toggle Navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        {/* List of Links */}
        <div className="collapse navbar-collapse" id="mainNavigation">
          <ul className="navbar-nav me-auto">
            {/* Game Inventory Dropdown */}
            <li className="nav-item" id="inventoryDropDown">
              <div className="dropdown">
                <button className="btn btn-success dropdown-toggle" id="dropdownMenuButton" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Game Inventory</button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <NavLink className="dropdown-item" to="inventory/developers">Developers</NavLink>
                  <NavLink className="dropdown-item" to="inventory/publishers">Publishers</NavLink>
                  <NavLink className="dropdown-item" to="inventory/platforms">Platforms</NavLink>
                  <NavLink className="dropdown-item" to="inventory/genres">Genres</NavLink>
                  <NavLink className="dropdown-item" to="inventory/tags">Tags</NavLink>
                  <NavLink className="dropdown-item" to="inventory">Games</NavLink>
                </div>
              </div>
            </li>
            {/* Employees Dropdown */}
            <li className="nav-item" id="inventoryDropDown">
              <div className="dropdown">
                <button className="btn btn-success dropdown-toggle" id="dropdownMenuButton" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Employees</button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <NavLink className="dropdown-item" to="employees/register">Register an Employee</NavLink>
                  <NavLink className="dropdown-item" to="employees">Employee Purchase</NavLink>
                </div>
              </div>
            </li>
            {/* Customers Dropdown */}
            <li className="nav-item" id="inventoryDropDown">
              <div className="dropdown">
                <button className="btn btn-success dropdown-toggle" id="dropdownMenuButton" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Customers</button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <NavLink className="dropdown-item" to="customers">Customer Purchase</NavLink>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    </div>
  )
}

export default Nav;
