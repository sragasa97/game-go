import React, {useState} from 'react'

function DeveloperForm() {
    const [submitted, setSubmitted] = useState(false)
    const [formData, setFormData] = useState({
        name: "",
        rawg_id: ""
    })

    const handleFormChange = (e) => {
        const input = e.target.name;
        const value = e.target.value;

        setFormData({...formData, [input]:value})
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const developerURL = "http://localhost:8100/api/developers/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json"
            }
        };

        const response = await fetch(developerURL, fetchConfig)

        if (response.ok) {
            setFormData({
                name: "",
                rawg_id: ""
            })
            setSubmitted(true)
        }
    }

    const handleFormReset = () => {
        setSubmitted(false)
    }


    if (!(submitted)) {
        return (
        <div className="container mx-5 my-5">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h2 className="py-2" style={{textAlign: "center"}}>Add a Developer</h2>
                        <form id="developer-form" onSubmit={handleSubmit}>
                            <div className="form-floating mb-3">
                                <input type="text" value={formData.name} onChange={handleFormChange} placeholder="name" required name="name" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        )
    } else {
        return (
        <div className="my-5 container">
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <div className="d-grid gap-2">
                        <div className="alert alert-success mb-0" id="success-message">
                            <h2 style={{textAlign:'center'}}>Success!</h2>
                            <h5 style={{textAlign:'center'}}>Developer has been registered.</h5>
                        </div>
                        <div className="d-grid gap-2" style={{padding:'2px'}}>
                            <button onClick={handleFormReset} className="btn btn-primary">Register Another Developer</button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        )
    }


}

export default DeveloperForm;
