import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav.jsx';
import MainPage from './MainPage.jsx';
import DeveloperForm from './components/Forms/DeveloperForm.jsx';


function App() {
  return (
    <BrowserRouter>
      <div className="container mx-0">
      <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="inventory">
            <Route index element={<MainPage />} />
            <Route path="developers" element={<DeveloperForm />} />
            <Route path="publishers" element={<MainPage />} />
            <Route path="platforms" element={<MainPage />} />
            <Route path="genres" element={<MainPage />} />
            <Route path="tags" element={<MainPage />} />
          </Route>
          <Route path="employees">
            <Route index element={<MainPage />} />
            <Route path="register" element={<MainPage />} />
            </Route>
          <Route path="customers" element={<MainPage />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
