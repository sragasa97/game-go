import {ReactComponent as GameGoLogo} from './static/gamego-high-resolution-logo-color-on-transparent-background.svg'

function MainPage() {
  return (
    <div className="row mainfix">
    <div className="px-5 py-2 mx-5 my-4 text-left">
      <GameGoLogo className="gamegologo d-inline-block"/>
      <h3 style={{fontFamily: "Bowlby One SC", letterSpacing: "0.4rem", fontSize: "1.5em"}}>
        It's time to level up! <br />
        Bring your company's <br />
        inventory management to <br />
        the next stage. 🎮🕹️</h3>
    </div>
    </div>
  );
}

export default MainPage;
