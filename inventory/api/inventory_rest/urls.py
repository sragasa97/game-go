from django.urls import path

from .views import (
    api_developers,
    api_developer_detail,
    api_publishers,
    api_publisher_detail,
    api_platforms,
    api_platform_detail,
    api_genres,
    api_genre_detail,
    api_tags,
    api_tag_detail,
    api_games,
    api_game_detail,
)

urlpatterns = [
    path("developers/", api_developers, name="api_developers"),
    path("developers/<str:slug>/", api_developer_detail, name="api_developer_detail"),
    path("publishers/", api_publishers, name="api_publishers"),
    path("publishers/<str:slug>/", api_publisher_detail, name="api_publisher_detail"),
    path("platforms/", api_platforms, name="api_platforms"),
    path("platforms/<str:slug>/", api_platform_detail, name="api_platform_detail"),
    path("genres/", api_genres, name="api_genres"),
    path("genres/<str:slug>/", api_genre_detail, name="api_genre_detail"),
    path("tags/", api_tags, name="api_tags"),
    path("tags/<str:slug>/", api_tag_detail, name="api_tag_detail"),
    path("games/", api_games, name="api_games"),
    path("games/<str:slug>/", api_game_detail, name="api_game_detail"),
]
