from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    DeveloperEncoder,
    PublisherEncoder,
    PlatformEncoder,
    GenreEncoder,
    TagEncoder,
    GameEncoder,
)

from .models import Developer, Publisher, Platform, Genre, Tag, Game

# ------------------------------ DEVELOPER VIEWS ------------------------------ #


@require_http_methods(["GET", "POST"])
def api_developers(request):
    if request.method == "GET":
        developers = Developer.objects.all()
        return JsonResponse({"developers": developers}, encoder=DeveloperEncoder)
    else:
        try:
            content = json.loads(request.body)

            developer = Developer.objects.create(**content)

            return JsonResponse(developer, encoder=DeveloperEncoder, safe=False)
        except:
            response = JsonResponse(
                {"message": "Could not add developer to inventory."}, status=400
            )
            return response


@require_http_methods(["GET", "DELETE"])
def api_developer_detail(request, slug):
    if request.method == "GET":
        try:
            developer = Developer.objects.get(slug=slug)
            return JsonResponse(developer, encoder=DeveloperEncoder, safe=False)
        except Developer.DoesNotExist:
            return JsonResponse({"message": "Invalid Developer Name"}, status=404)
    else:
        count, items = Developer.objects.filter(slug=slug).delete()

        if count > 0:
            return JsonResponse({"deleted": {"successful?": count > 0, "items": items}})
        else:
            return JsonResponse(
                {
                    "deleted": {
                        "successful?": count > 0,
                        "message": "Developer could not be deleted. Either invalid developer name or the developer has already been deleted.",
                    }
                },
                status=404,
            )


# ------------------------------ PUBLISHER VIEWS ------------------------------ #


@require_http_methods(["GET", "POST"])
def api_publishers(request):
    if request.method == "GET":
        publishers = Publisher.objects.all()
        return JsonResponse({"publishers": publishers}, encoder=PublisherEncoder)
    else:
        try:
            content = json.loads(request.body)

            publisher = Publisher.objects.create(**content)
            return JsonResponse(publisher, encoder=PublisherEncoder, safe=False)
        except:
            response = JsonResponse(
                {"message": "Could not add publisher to inventory."}, status=400
            )
            return response


@require_http_methods(["GET", "DELETE"])
def api_publisher_detail(request, slug):
    if request.method == "GET":
        try:
            publisher = Publisher.objects.get(slug=slug)
            return JsonResponse(publisher, encoder=PublisherEncoder, safe=False)
        except Publisher.DoesNotExist:
            return JsonResponse({"message": "Invalid Publisher Name"}, status=404)
    else:
        count, items = Publisher.objects.filter(slug=slug).delete()

        if count > 0:
            return JsonResponse({"deleted": {"successful?": count > 0, "items": items}})
        else:
            return JsonResponse(
                {
                    "deleted": {
                        "successful?": count > 0,
                        "message": "Publisher could not be deleted. Either invalid publisher name or the publisher has already been deleted.",
                    }
                },
                status=404,
            )


# ------------------------------ PLATFORM VIEWS ------------------------------ #


@require_http_methods(["GET", "POST"])
def api_platforms(request):
    if request.method == "GET":
        platforms = Platform.objects.all()
        return JsonResponse({"platforms": platforms}, encoder=PlatformEncoder)
    else:
        try:
            content = json.loads(request.body)

            platform = Platform.objects.create(**content)
            return JsonResponse(platform, encoder=PlatformEncoder, safe=False)
        except:
            response = JsonResponse(
                {"message": "Could not add platform to inventory."}, status=400
            )
            return response


@require_http_methods(["GET", "DELETE"])
def api_platform_detail(request, slug):
    if request.method == "GET":
        try:
            platform = Platform.objects.get(slug=slug)
            return JsonResponse(platform, encoder=PlatformEncoder, safe=False)
        except Platform.DoesNotExist:
            return JsonResponse({"message": "Invalid Platform Name"}, status=404)
    else:
        count, items = Platform.objects.filter(slug=slug).delete()

        if count > 0:
            return JsonResponse({"deleted": {"successful?": count > 0, "items": items}})
        else:
            return JsonResponse(
                {
                    "deleted": {
                        "successful?": count > 0,
                        "message": "Platform could not be deleted. Either invalid platform name or the platform has already been deleted.",
                    }
                },
                status=404,
            )


# ------------------------------ GENRE VIEWS ------------------------------ #


@require_http_methods(["GET", "POST"])
def api_genres(request):
    if request.method == "GET":
        genres = Genre.objects.all()
        return JsonResponse({"genres": genres}, encoder=GenreEncoder)
    else:
        try:
            content = json.loads(request.body)

            genre = Genre.objects.create(**content)
            return JsonResponse(genre, encoder=GenreEncoder, safe=False)
        except:
            response = JsonResponse(
                {"message": "Could not add genre to inventory."}, status=400
            )
            return response


@require_http_methods(["GET", "DELETE"])
def api_genre_detail(request, slug):
    if request.method == "GET":
        try:
            genre = Genre.objects.get(slug=slug)
            return JsonResponse(genre, encoder=GenreEncoder, safe=False)
        except Genre.DoesNotExist:
            return JsonResponse({"message": "Invalid Genre Name"}, status=404)
    else:
        count, items = Genre.objects.filter(slug=slug).delete()

        if count > 0:
            return JsonResponse({"deleted": {"successful?": count > 0, "items": items}})
        else:
            return JsonResponse(
                {
                    "deleted": {
                        "successful?": count > 0,
                        "message": "Genre could not be deleted. Either invalid genre name or the genre has already been deleted.",
                    }
                },
                status=404,
            )


# ------------------------------ TAG VIEWS ------------------------------ #


@require_http_methods(["GET", "POST"])
def api_tags(request):
    if request.method == "GET":
        tags = Tag.objects.all()
        return JsonResponse({"tags": tags}, encoder=TagEncoder)
    else:
        try:
            content = json.loads(request.body)

            tag = Tag.objects.create(**content)
            return JsonResponse(tag, encoder=TagEncoder, safe=False)
        except:
            response = JsonResponse(
                {"message": "Could not add tag to inventory."}, status=400
            )
            return response


@require_http_methods(["GET", "DELETE"])
def api_tag_detail(request, slug):
    if request.method == "GET":
        try:
            tag = Tag.objects.get(slug=slug)
            return JsonResponse(tag, encoder=TagEncoder, safe=False)
        except Tag.DoesNotExist:
            return JsonResponse({"message": "Invalid Tag Name"}, status=404)
    else:
        count, items = Tag.objects.filter(slug=slug).delete()

        if count > 0:
            return JsonResponse({"deleted": {"successful?": count > 0, "items": items}})
        else:
            return JsonResponse(
                {
                    "deleted": {
                        "successful?": count > 0,
                        "message": "Tag could not be deleted. Either invalid tag name or the tag has already been deleted.",
                    }
                },
                status=404,
            )


# ------------------------------ GAME VIEWS ------------------------------ #


@require_http_methods(["GET", "POST"])
def api_games(request):
    if request.method == "GET":
        games = Game.objects.all()
        return JsonResponse({"games": games}, encoder=GameEncoder)
    else:
        try:
            content = json.loads(request.body)
            related_fields = {}

            if "developer" in content:
                try:
                    developers = []
                    for dev in content["developer"]:
                        developer = Developer.objects.get(slug=dev)
                        developers.append(developer)
                    related_fields["developer"] = developers
                    del content["developer"]
                except Developer.DoesNotExist:
                    return JsonResponse({"message": "Invalid developer name"})

            if "publisher" in content:
                try:
                    publishers = []
                    for pub in content["publisher"]:
                        publisher = Publisher.objects.get(slug=pub)
                        publishers.append(publisher)
                    related_fields["publisher"] = publishers
                    del content["publisher"]
                except Publisher.DoesNotExist:
                    return JsonResponse({"message": "Invalid publisher name"})

            if "platform" in content:
                try:
                    platforms = []
                    for plat in content["platform"]:
                        platform = Platform.objects.get(slug=plat)
                        platforms.append(platform)
                    related_fields["platform"] = platforms
                    del content["platform"]
                except Platform.DoesNotExist:
                    return JsonResponse({"message": "Invalid platform name"})

            if "genre" in content:
                try:
                    genres = []
                    for gen in content["genre"]:
                        genre = Genre.objects.get(slug=gen)
                        genres.append(genre)
                    related_fields["genre"] = genres
                    del content["genre"]
                except Genre.DoesNotExist:
                    return JsonResponse({"message": "Invalid genre name"})

            if "tag" in content:
                try:
                    tags = []
                    for tg in content["tag"]:
                        tag = Tag.objects.get(slug=tg)
                        tags.append(tag)
                    related_fields["tag"] = tags
                    del content["tag"]
                except Tag.DoesNotExist:
                    return JsonResponse({"message": "Invalid tag name"})
            try:
                game = Game.objects.create(**content)

                for field in related_fields.keys():
                    if field == "developer":
                        for input in related_fields["developer"]:
                            game.developer.add(input)
                    elif field == "publisher":
                        for input in related_fields["publisher"]:
                            game.publisher.add(input)
                    elif field == "platform":
                        for input in related_fields["platform"]:
                            game.platform.add(input)
                    elif field == "genre":
                        for input in related_fields["genre"]:
                            game.genre.add(input)
                    elif field == "tag":
                        for input in related_fields["tag"]:
                            game.tag.add(input)

            except Exception as e:
                return JsonResponse({"message": f"Error: {e}"})

            return JsonResponse(game, encoder=GameEncoder, safe=False)
        except:
            response = JsonResponse(
                {"message": "Could not add game to inventory."}, status=400
            )
            return response


@require_http_methods(["GET", "DELETE", "PUT"])
def api_game_detail(request, slug):
    if request.method == "GET":
        try:
            game = Game.objects.get(slug=slug)
            return JsonResponse(game, encoder=GameEncoder, safe=False)
        except Game.DoesNotExist:
            return JsonResponse({"message": "Invalid Game Title"}, status=404)
    elif request.method == "DELETE":
        count, items = Game.objects.filter(slug=slug).delete()

        if count > 0:
            return JsonResponse({"deleted": {"successful?": count > 0, "items": items}})
        else:
            return JsonResponse(
                {
                    "deleted": {
                        "successful?": count > 0,
                        "message": "Game could not be deleted. Either invalid game title or the game has already been deleted.",
                    }
                },
                status=404,
            )
    else:
        content = json.loads(request.body)
        related_fields = {}

        try:
            if "developer" in content:
                try:
                    developers = []
                    for dev in content["developer"]:
                        developer = Developer.objects.get(slug=dev)
                        developers.append(developer)
                    related_fields["developer"] = developers
                    del content["developer"]
                except Developer.DoesNotExist:
                    return JsonResponse({"message": "Invalid developer name"})

            if "publisher" in content:
                try:
                    publishers = []
                    for pub in content["publisher"]:
                        publisher = Publisher.objects.get(slug=pub)
                        publishers.append(publisher)
                    related_fields["publisher"] = publishers
                    del content["publisher"]
                except Publisher.DoesNotExist:
                    return JsonResponse({"message": "Invalid publisher name"})

            if "platform" in content:
                try:
                    platforms = []
                    for plat in content["platform"]:
                        platform = Platform.objects.get(slug=plat)
                        platforms.append(platform)
                    related_fields["platform"] = platforms
                    del content["platform"]
                except Platform.DoesNotExist:
                    return JsonResponse({"message": "Invalid platform name"})

            if "genre" in content:
                try:
                    genres = []
                    for gen in content["genre"]:
                        genre = Genre.objects.get(slug=gen)
                        genres.append(genre)
                    related_fields["genre"] = genres
                    del content["genre"]
                except Genre.DoesNotExist:
                    return JsonResponse({"message": "Invalid genre name"})

            if "tag" in content:
                try:
                    tags = []
                    for tg in content["tag"]:
                        tag = Tag.objects.get(slug=tg)
                        tags.append(tag)
                    related_fields["tag"] = tags
                    del content["tag"]
                except Tag.DoesNotExist:
                    return JsonResponse({"message": "Invalid tag name"})

            Game.objects.filter(slug=slug).update(**content)

            game = Game.objects.get(slug=slug)

            if len(related_fields) > 0:
                for field in related_fields.keys():
                    if field == "developer":
                        for input in related_fields["developer"]:
                            game.developer.add(input)
                    elif field == "publisher":
                        for input in related_fields["publisher"]:
                            game.publisher.add(input)
                    elif field == "platform":
                        for input in related_fields["platform"]:
                            game.platform.add(input)
                    elif field == "genre":
                        for input in related_fields["genre"]:
                            game.genre.add(input)
                    elif field == "tag":
                        for input in related_fields["tag"]:
                            game.tag.add(input)

            return JsonResponse(game, encoder=GameEncoder, safe=False)

        except:
            response = JsonResponse(
                {"message": "Could not update game details at this time."}, status=400
            )
            return response
