# Generated by Django 4.0.3 on 2023-04-02 18:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory_rest', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Developer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, unique=True)),
                ('slug', models.CharField(max_length=200, unique=True)),
                ('rawg_id', models.CharField(max_length=250, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, unique=True)),
                ('slug', models.CharField(max_length=200, unique=True)),
                ('description', models.TextField(null=True)),
                ('released', models.DateTimeField(null=True)),
                ('image', models.URLField(null=True)),
                ('website', models.URLField(null=True)),
                ('rating', models.PositiveSmallIntegerField(null=True)),
                ('copies', models.PositiveSmallIntegerField(default=0)),
                ('rawg_id', models.CharField(max_length=250, unique=True)),
                ('developer', models.ManyToManyField(related_name='game', to='inventory_rest.developer')),
            ],
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, unique=True)),
                ('slug', models.CharField(max_length=200, unique=True)),
                ('rawg_id', models.CharField(max_length=250, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Platform',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, unique=True)),
                ('slug', models.CharField(max_length=200, unique=True)),
                ('image', models.URLField(null=True)),
                ('rawg_id', models.CharField(max_length=250, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Publisher',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, unique=True)),
                ('slug', models.CharField(max_length=200, unique=True)),
                ('domain', models.CharField(max_length=250, null=True)),
                ('rawg_id', models.CharField(max_length=250, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, unique=True)),
                ('slug', models.CharField(max_length=200, unique=True)),
                ('rawg_id', models.CharField(max_length=250, unique=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='vehiclemodel',
            name='manufacturer',
        ),
        migrations.DeleteModel(
            name='Automobile',
        ),
        migrations.DeleteModel(
            name='Manufacturer',
        ),
        migrations.DeleteModel(
            name='VehicleModel',
        ),
        migrations.AddField(
            model_name='game',
            name='genre',
            field=models.ManyToManyField(related_name='game', to='inventory_rest.genre'),
        ),
        migrations.AddField(
            model_name='game',
            name='platform',
            field=models.ManyToManyField(related_name='game', to='inventory_rest.platform'),
        ),
        migrations.AddField(
            model_name='game',
            name='publisher',
            field=models.ManyToManyField(related_name='game', to='inventory_rest.publisher'),
        ),
        migrations.AddField(
            model_name='game',
            name='tag',
            field=models.ManyToManyField(related_name='game', to='inventory_rest.tag'),
        ),
    ]
