from django.contrib import admin

from .models import Developer, Publisher, Platform, Genre, Tag, Game


admin.site.register(Developer)
admin.site.register(Publisher)
admin.site.register(Platform)
admin.site.register(Genre)
admin.site.register(Tag)
admin.site.register(Game)
