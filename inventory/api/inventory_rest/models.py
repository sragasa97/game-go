from django.db import models
from django.urls import reverse
from django.utils.text import slugify


class Developer(models.Model):
    name = models.CharField(max_length=200, unique=True)
    slug = models.CharField(max_length=200, null=False, unique=True)
    rawg_id = models.CharField(max_length=250, unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_developer_detail", kwargs={"slug": self.slug})

    def save(
        self,
        force_insert=False,
        force_update=False,
        using=None,
        update_fields=None,
        *args,
        **kwargs
    ):
        if not self.slug:
            self.slug = slugify(self.name)
        if update_fields is not None and "name" in update_fields:
            update_fields = {"slug"}.union(update_fields)
        super().save(
            force_insert=force_insert,
            force_update=force_update,
            using=using,
            update_fields=update_fields,
            *args,
            **kwargs
        )


class Publisher(models.Model):
    name = models.CharField(max_length=200, unique=True)
    slug = models.CharField(max_length=200, null=False, unique=True)
    rawg_id = models.CharField(max_length=250, unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_publisher_detail", kwargs={"slug": self.slug})

    def save(
        self,
        force_insert=False,
        force_update=False,
        using=None,
        update_fields=None,
        *args,
        **kwargs
    ):
        if not self.slug:
            self.slug = slugify(self.name)
        if update_fields is not None and "name" in update_fields:
            update_fields = {"slug"}.union(update_fields)
        super().save(
            force_insert=force_insert,
            force_update=force_update,
            using=using,
            update_fields=update_fields,
            *args,
            **kwargs
        )


class Platform(models.Model):
    name = models.CharField(max_length=200, unique=True)
    slug = models.CharField(max_length=200, null=False, unique=True)
    image = models.URLField(null=True)
    rawg_id = models.CharField(max_length=250, unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_platform_detail", kwargs={"slug": self.slug})

    def save(
        self,
        force_insert=False,
        force_update=False,
        using=None,
        update_fields=None,
        *args,
        **kwargs
    ):
        if not self.slug:
            self.slug = slugify(self.name)
        if update_fields is not None and "name" in update_fields:
            update_fields = {"slug"}.union(update_fields)
        super().save(
            force_insert=force_insert,
            force_update=force_update,
            using=using,
            update_fields=update_fields,
            *args,
            **kwargs
        )


class Genre(models.Model):
    name = models.CharField(max_length=200, unique=True)
    slug = models.CharField(max_length=200, null=False, unique=True)
    rawg_id = models.CharField(max_length=250, unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_genre_detail", kwargs={"slug": self.slug})

    def save(
        self,
        force_insert=False,
        force_update=False,
        using=None,
        update_fields=None,
        *args,
        **kwargs
    ):
        if not self.slug:
            self.slug = slugify(self.name)
        if update_fields is not None and "name" in update_fields:
            update_fields = {"slug"}.union(update_fields)
        super().save(
            force_insert=force_insert,
            force_update=force_update,
            using=using,
            update_fields=update_fields,
            *args,
            **kwargs
        )


class Tag(models.Model):
    name = models.CharField(max_length=200, unique=True)
    slug = models.CharField(max_length=200, null=False, unique=True)
    rawg_id = models.CharField(max_length=250, unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_tag_detail", kwargs={"slug": self.slug})

    def save(
        self,
        force_insert=False,
        force_update=False,
        using=None,
        update_fields=None,
        *args,
        **kwargs
    ):
        if not self.slug:
            self.slug = slugify(self.name)
        if update_fields is not None and "name" in update_fields:
            update_fields = {"slug"}.union(update_fields)
        super().save(
            force_insert=force_insert,
            force_update=force_update,
            using=using,
            update_fields=update_fields,
            *args,
            **kwargs
        )


class Game(models.Model):
    title = models.CharField(max_length=200, unique=True)
    slug = models.CharField(max_length=200, null=False, unique=True)
    description = models.TextField(null=True)
    released = models.DateTimeField(null=True)
    image = models.URLField(null=True)
    website = models.URLField(null=True)
    rating = models.PositiveSmallIntegerField(null=True)
    copies = models.PositiveSmallIntegerField(default=0)
    rawg_id = models.CharField(max_length=250, unique=True)

    developer = models.ManyToManyField(Developer, related_name="game")
    publisher = models.ManyToManyField(Publisher, related_name="game")
    platform = models.ManyToManyField(Platform, related_name="game")
    genre = models.ManyToManyField(Genre, related_name="game")
    tag = models.ManyToManyField(Tag, related_name="game")

    def __str__(self):
        return self.title

    def get_api_url(self):
        return reverse("api_game_detail", kwargs={"slug": self.slug})

    def save(
        self,
        force_insert=False,
        force_update=False,
        using=None,
        update_fields=None,
        *args,
        **kwargs
    ):
        if not self.slug:
            self.slug = slugify(self.title)
        if update_fields is not None and "title" in update_fields:
            update_fields = {"slug"}.union(update_fields)
        super().save(
            force_insert=force_insert,
            force_update=force_update,
            using=using,
            update_fields=update_fields,
            *args,
            **kwargs
        )
