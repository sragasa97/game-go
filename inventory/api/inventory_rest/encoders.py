from common.json import ModelEncoder

from .models import Developer, Publisher, Platform, Genre, Tag, Game


class DeveloperEncoder(ModelEncoder):
    model = Developer
    properties = ["id", "name", "slug", "rawg_id"]


class PublisherEncoder(ModelEncoder):
    model = Publisher
    properties = [
        "id",
        "name",
        "slug",
        "rawg_id",
    ]


class PlatformEncoder(ModelEncoder):
    model = Platform
    properties = [
        "id",
        "name",
        "slug",
        "image",
        "rawg_id",
    ]


class GenreEncoder(ModelEncoder):
    model = Genre
    properties = [
        "id",
        "name",
        "slug",
        "rawg_id",
    ]


class TagEncoder(ModelEncoder):
    model = Tag
    properties = [
        "id",
        "name",
        "slug",
        "rawg_id",
    ]


class GameEncoder(ModelEncoder):
    model = Game
    properties = [
        "id",
        "title",
        "slug",
        "description",
        "released",
        "image",
        "website",
        "rating",
        "developer",
        "publisher",
        "platform",
        "genre",
        "tag",
        "copies",
        "rawg_id",
    ]
    encoders = {
        "developer": DeveloperEncoder(),
        "publisher": PublisherEncoder(),
        "platform": PlatformEncoder(),
        "genre": GenreEncoder(),
        "tag": TagEncoder(),
    }
