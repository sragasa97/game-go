from json import JSONEncoder
from django.urls import NoReverseMatch
from django.db.models import QuerySet, Manager
from decimal import Decimal
from django.forms.models import model_to_dict
from datetime import datetime, date, time
from inventory_rest.models import Developer, Publisher, Platform, Genre, Tag, Game


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        elif isinstance(o, date):
            return o.isoformat()
        elif isinstance(o, time):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ManyToManyEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, Manager):
            return o.all()
        else:
            return super().default(o)


class FieldsEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, Developer):
            return model_to_dict(o)
        elif isinstance(o, Publisher):
            return model_to_dict(o)
        elif isinstance(o, Platform):
            return model_to_dict(o)
        elif isinstance(o, Genre):
            return model_to_dict(o)
        elif isinstance(o, Tag):
            return model_to_dict(o)
        else:
            return super().default(o)


class DecimalEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, Decimal):
            return float(o)


class ModelEncoder(
    DateEncoder,
    QuerySetEncoder,
    ManyToManyEncoder,
    FieldsEncoder,
    DecimalEncoder,
    JSONEncoder,
):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                try:
                    d["href"] = o.get_api_url()
                except NoReverseMatch:
                    pass
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
