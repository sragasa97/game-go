from django.urls import path

from .views import (
    api_employees,
    api_employee_detail,
    api_employee_purchases,
    api_employee_purchase_detail,
)

urlpatterns = [
    path("employees/", api_employees, name="api_employees"),
    path("employees/<int:employee_number>/", api_employee_detail),
    path("employee_purchases/", api_employee_purchases, name="api_employee_purchases"),
    path(
        "employee_purchases/<int:id>/",
        api_employee_purchase_detail,
        name="api_employee_purchase_detail",
    ),
]
