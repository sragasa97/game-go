from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from .encoders import EmployeeEncoder, PurchaseEncoder

from .models import GameVO, Employee, Purchase


@require_http_methods(["GET", "POST"])
def api_employees(request):
    if request.method == "GET":
        employees = Employee.objects.all()

        return JsonResponse({"employees": employees}, encoder=EmployeeEncoder)
    else:
        content = json.loads(request.body)

        employee = Employee.objects.create(**content)

        return JsonResponse(employee, encoder=EmployeeEncoder, safe=False)


@require_http_methods(["GET", "DELETE", "PUT"])
def api_employee_detail(request, employee_number):
    if request.method == "GET":
        try:
            employee = Employee.objects.get(employee_number=employee_number)

            return JsonResponse({"employee": employee}, encoder=EmployeeEncoder)
        except Employee.DoesNotExist:
            return JsonResponse({"message": "Invalid Employee Number"}, status=404)
    elif request.method == "DELETE":
        count, items = Employee.objects.filter(employee_number=employee_number).delete()

        return JsonResponse({"deleted": {"successful?": count > 0, "items": items}})
    else:
        content = json.loads(request.body)

        Employee.objects.filter(employee_number=employee_number).update(**content)

        employee = Employee.objects.get(employee_number=employee_number)
        return JsonResponse(employee, encoder=EmployeeEncoder, safe=False)


@require_http_methods(["GET", "POST"])
def api_employee_purchases(request):
    if request.method == "GET":
        purchases = Purchase.objects.all()

        return JsonResponse({"purchases": purchases}, encoder=PurchaseEncoder)
    else:
        content = json.loads(request.body)

        if "game" in content:
            try:
                game = GameVO.objects.get(id=content["game"])
                content["game"] = game
            except GameVO.DoesNotExist:
                return JsonResponse({"message": "Invalid Game ID"})

        if "employee" in content:
            try:
                employee = Employee.objects.get(employee_number=content["employee"])
                content["employee"] = employee
            except Employee.DoesNotExist:
                return JsonResponse({"message": "Invalid Employee Number"})

        purchase = Purchase.objects.create(**content)
        return JsonResponse(purchase, encoder=PurchaseEncoder, safe=False)


@require_http_methods(["GET", "DELETE", "PUT"])
def api_employee_purchase_detail(request, id):
    if request.method == "GET":
        purchase = Purchase.objects.get(id=id)

        return JsonResponse({"purchase": purchase}, encoder=PurchaseEncoder)
    elif request.method == "DELETE":
        count, items = Purchase.objects.filter(id=id).delete()

        return JsonResponse({"deleted": {"successful?": count > 0, "items": items}})
    else:
        content = json.loads(request.body)

        if "game" in content:
            try:
                game = GameVO.objects.get(id=content["game"])
                content["game"] = game
            except GameVO.DoesNotExist:
                return JsonResponse({"message": "Invalid Game ID"})

        if "employee" in content:
            try:
                employee = Employee.objects.get(employee_number=content["employee"])
                content["employee"] = employee
            except Employee.DoesNotExist:
                return JsonResponse({"message": "Invalid Employee Number"})

        Purchase.objects.filter(id=id).update(**content)

        purchase = Purchase.objects.get(id=id)
        return JsonResponse(purchase, encoder=PurchaseEncoder, safe=False)
