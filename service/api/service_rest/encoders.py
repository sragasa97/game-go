from common.json import ModelEncoder

from .models import (
    DeveloperVO,
    PublisherVO,
    PlatformVO,
    GenreVO,
    TagVO,
    GameVO,
    Employee,
    Purchase,
)


class DeveloperVOEncoder(ModelEncoder):
    model = DeveloperVO
    properties = ["import_href", "id", "name", "slug", "rawg_id"]


class PublisherVOEncoder(ModelEncoder):
    model = PublisherVO
    properties = [
        "import_href",
        "id",
        "name",
        "slug",
        "rawg_id",
    ]


class PlatformVOEncoder(ModelEncoder):
    model = PlatformVO
    properties = [
        "import_href",
        "id",
        "name",
        "slug",
        "image",
        "rawg_id",
    ]


class GenreVOEncoder(ModelEncoder):
    model = GenreVO
    properties = [
        "import_href",
        "id",
        "name",
        "slug",
        "rawg_id",
    ]


class TagVOEncoder(ModelEncoder):
    model = TagVO
    properties = [
        "import_href",
        "id",
        "name",
        "slug",
        "rawg_id",
    ]


class GameVOEncoder(ModelEncoder):
    model = GameVO
    properties = [
        "import_href",
        "id",
        "title",
        "slug",
        "description",
        "released",
        "image",
        "website",
        "rating",
        "developer",
        "publisher",
        "platform",
        "genre",
        "tag",
        "copies",
        "rawg_id",
    ]
    encoders = {
        "developer": DeveloperVOEncoder(),
        "publisher": PublisherVOEncoder(),
        "platform": PlatformVOEncoder(),
        "genre": GenreVOEncoder(),
        "tag": TagVOEncoder(),
    }


class EmployeeEncoder(ModelEncoder):
    model = Employee
    properties = [
        "name",
        "employee_number",
    ]


class PurchaseEncoder(ModelEncoder):
    model = Purchase
    properties = [
        "game",
        "employee",
        "cost",
        "date",
    ]
    encoders = {
        "game": GameVOEncoder(),
        "employee": EmployeeEncoder(),
    }
