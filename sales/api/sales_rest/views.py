from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from .encoders import PurchaseEncoder

from .models import GameVO, Purchase


@require_http_methods(["GET", "POST"])
def api_purchases(request):
    if request.method == "GET":
        purchases = Purchase.objects.all()

        return JsonResponse({"purchases": purchases}, encoder=PurchaseEncoder)
    else:
        content = json.loads(request.body)

        if "game" in content:
            try:
                game = GameVO.objects.get(id=content["game"])
                content["game"] = game

            except GameVO.DoesNotExist:
                return JsonResponse({"message": "Invalid Game ID"})

        purchase = Purchase.objects.create(**content)

        return JsonResponse(purchase, encoder=PurchaseEncoder, safe=False)


@require_http_methods(["GET", "DELETE", "PUT"])
def api_purchase_detail(request, id):
    if request.method == "GET":
        try:
            purchase = Purchase.objects.get(id=id)

            return JsonResponse({"purchase": purchase}, encoder=PurchaseEncoder)
        except Purchase.DoesNotExist:
            return JsonResponse({"message": "Invalid Purchase ID"}, status=404)
    elif request.method == "DELETE":
        count, items = Purchase.objects.filter(id=id).delete()

        return JsonResponse({"deleted": {"successful?": count > 0, "items": items}})
    else:
        content = json.loads(request.body)

        if "game" in content:
            try:
                game = GameVO.objects.get(id=content["game"])
                content["game"] = game
            except GameVO.DoesNotExist:
                return JsonResponse({"message": "Invalid Game ID"})

        Purchase.objects.filter(id=id).update(**content)

        purchase = Purchase.objects.get(id=id)
        return JsonResponse(purchase, encoder=PurchaseEncoder, safe=False)
