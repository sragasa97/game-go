from django.urls import path

from .views import api_purchases, api_purchase_detail

urlpatterns = [
    path("sales/", api_purchases, name="api_purchases"),
    path("sales/<int:id>/", api_purchase_detail, name="api_purchase_detail"),
]
