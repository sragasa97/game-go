from django.contrib import admin
from .models import (
    DeveloperVO,
    PublisherVO,
    PlatformVO,
    GenreVO,
    TagVO,
    GameVO,
    Purchase,
)

admin.site.register(DeveloperVO)
admin.site.register(PublisherVO)
admin.site.register(PlatformVO)
admin.site.register(GenreVO)
admin.site.register(TagVO)
admin.site.register(GameVO)
admin.site.register(Purchase)
