from django.db import models
from django.urls import reverse

# -------------------- VALUE OBJECT MODELS -------------------- #


class DeveloperVO(models.Model):
    name = models.CharField(max_length=200, unique=True)
    slug = models.CharField(max_length=200, null=False, unique=True)
    rawg_id = models.CharField(max_length=250, unique=True)
    import_href = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class PublisherVO(models.Model):
    name = models.CharField(max_length=200, unique=True)
    slug = models.CharField(max_length=200, null=False, unique=True)
    rawg_id = models.CharField(max_length=250, unique=True)
    import_href = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class PlatformVO(models.Model):
    name = models.CharField(max_length=200, unique=True)
    slug = models.CharField(max_length=200, null=False, unique=True)
    image = models.URLField(null=True)
    rawg_id = models.CharField(max_length=250, unique=True)
    import_href = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class GenreVO(models.Model):
    name = models.CharField(max_length=200, unique=True)
    slug = models.CharField(max_length=200, null=False, unique=True)
    rawg_id = models.CharField(max_length=250, unique=True)
    import_href = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class TagVO(models.Model):
    name = models.CharField(max_length=200, unique=True)
    slug = models.CharField(max_length=200, null=False, unique=True)
    rawg_id = models.CharField(max_length=250, unique=True)
    import_href = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class GameVO(models.Model):
    title = models.CharField(max_length=200, unique=True)
    slug = models.CharField(max_length=200, null=False, unique=True)
    description = models.TextField(null=True)
    released = models.DateTimeField(null=True)
    image = models.URLField(null=True)
    website = models.URLField(null=True)
    rating = models.PositiveSmallIntegerField(null=True)
    copies = models.PositiveSmallIntegerField(default=0)
    rawg_id = models.CharField(max_length=250, unique=True)
    import_href = models.CharField(max_length=200)

    developer = models.ManyToManyField(DeveloperVO, related_name="game")
    publisher = models.ManyToManyField(PublisherVO, related_name="game")
    platform = models.ManyToManyField(PlatformVO, related_name="game")
    genre = models.ManyToManyField(GenreVO, related_name="game")
    tag = models.ManyToManyField(TagVO, related_name="game")

    def __str__(self):
        return self.title


# -------------------- SALES MODELS -------------------- #


class Purchase(models.Model):
    game = models.ForeignKey(
        GameVO, related_name="purchase", on_delete=models.SET_NULL, null=True
    )
    cost = models.DecimalField(max_digits=6, decimal_places=2)
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return str(self.id)

    def get_api_url(self):
        return reverse("api_purchase_detail", kwargs={"id": self.id})
