import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

from sales_rest.models import (
    DeveloperVO,
    PublisherVO,
    PlatformVO,
    GenreVO,
    TagVO,
    GameVO,
)


def get_polls():
    def developer_poll():
        data = requests.get("http://inventory-api:8000/api/developers/")
        developers = json.loads(data.content)

        for developer in developers["developers"]:
            DeveloperVO.objects.update_or_create(
                import_href=developer["href"],
                defaults={
                    "id": developer["id"],
                    "name": developer["name"],
                    "slug": developer["slug"],
                    "rawg_id": developer["rawg_id"],
                },
            )

    def publisher_poll():
        data = requests.get("http://inventory-api:8000/api/publishers/")
        publishers = json.loads(data.content)

        for publisher in publishers["publishers"]:
            PublisherVO.objects.update_or_create(
                import_href=publisher["href"],
                defaults={
                    "id": publisher["id"],
                    "name": publisher["name"],
                    "slug": publisher["slug"],
                    "rawg_id": publisher["rawg_id"],
                },
            )

    def platform_poll():
        data = requests.get("http://inventory-api:8000/api/platforms/")
        platforms = json.loads(data.content)

        for platform in platforms["platforms"]:
            PlatformVO.objects.update_or_create(
                import_href=platform["href"],
                defaults={
                    "id": platform["id"],
                    "name": platform["name"],
                    "slug": platform["slug"],
                    "image": platform["image"],
                    "rawg_id": platform["rawg_id"],
                },
            )

    def genre_poll():
        data = requests.get("http://inventory-api:8000/api/genres/")
        genres = json.loads(data.content)

        for genre in genres["genres"]:
            GenreVO.objects.update_or_create(
                import_href=genre["href"],
                defaults={
                    "id": genre["id"],
                    "name": genre["name"],
                    "slug": genre["slug"],
                    "rawg_id": genre["rawg_id"],
                },
            )

    def tag_poll():
        data = requests.get("http://inventory-api:8000/api/tags/")
        tags = json.loads(data.content)

        for tag in tags["tags"]:
            TagVO.objects.update_or_create(
                import_href=tag["href"],
                defaults={
                    "id": tag["id"],
                    "name": tag["name"],
                    "slug": tag["slug"],
                    "rawg_id": tag["rawg_id"],
                },
            )

    def game_poll():
        data = requests.get("http://inventory-api:8000/api/games/")
        games = json.loads(data.content)

        for game in games["games"]:
            GameVO.objects.update_or_create(
                import_href=game["href"],
                defaults={
                    "id": game["id"],
                    "title": game["title"],
                    "slug": game["slug"],
                    "description": game["description"],
                    "released": game["released"],
                    "image": game["image"],
                    "website": game["website"],
                    "rating": game["rating"],
                    "copies": game["copies"],
                    "rawg_id": game["rawg_id"],
                },
            )

            devs, pubs, plats, gens, tags = [], [], [], [], []
            for dev in game["developer"]:
                devs.append(dev["id"])
            for pub in game["publisher"]:
                pubs.append(pub["id"])
            for plat in game["platform"]:
                plats.append(plat["id"])
            for gen in game["genre"]:
                gens.append(gen["id"])
            for tag in game["tag"]:
                tags.append(tag["id"])
            gamevo = GameVO.objects.get(import_href=game["href"])
            gamevo.developer.set(devs)
            gamevo.publisher.set(pubs)
            gamevo.platform.set(plats)
            gamevo.genre.set(gens)
            gamevo.tag.set(tags)

    developer_poll()
    publisher_poll()
    platform_poll()
    genre_poll()
    tag_poll()
    game_poll()


def poll():
    while True:
        print("Sales poller polling for data")
        try:
            get_polls()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
